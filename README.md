### This project is deprecated in favor of the [GitLab Environment Toolkit](https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit). 

---
# gitlab-provisioner

This project contains Terraform plans and Ansible playbooks that can be used to spin up a GitLab instance

### Issues

For any bugs, feature request, etc, please open an issue in the [omnibus-gitlab project](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=)

Please note this is not an officially supported product. Assistance will be provided on a best effort. 
