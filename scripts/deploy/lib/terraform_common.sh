# shellcheck shell=bash

# Terraform Configuration Values
export TF_VAR_google_project="${GCLOUD_PROJECT}"
export TF_VAR_google_zone=${GCLOUD_ZONE}
export TF_VAR_prefix="${TERRAFORM_PREFIX}"
export TF_VAR_ssh_public_key="${SSH_PUBLIC_KEY}"
export TF_VAR_ssh_user="${ORCHESTRATION_USER}"

# Functions
terraform_pretasks() {
    bucket_name="${1}"
    path_in_bucket="${2}"

    trap 'cd "${SRC_PATH}"' EXIT

    cd "${TERRAFORM_PATH}" || fail "Could not enter terraform directory"

    terraform init \
        -input=false \
        -backend-config "bucket=${bucket_name}" \
        -backend-config "prefix=${path_in_bucket}"
}
