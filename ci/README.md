# Continuous Integration Configuration

## Pipeline Environment Variables

Variables marked secret are set as private variables in the pipeline for the
continuous integration job. The public variables are set in the
`.gitlab-ci.yml` file.

|Name|Visibility|Description|Usage|
|-|-|-|-|
|`GCLOUD_CREDENTIALS_JSON`|Secret|[Credentials for Google Cloud in JSON format][gcred].|Stored into file path noted by `GOOGLE_APPLICATIONS_CREDENTIALS` at run time.|
|`STATE_GCS_BUCKET`|Secret|String identifying the [Google Storage Bucket](../terraform#useful-concepts).|Configures Google Cloud Backend `bucket` variable.|
|`GCLOUD_PROJECT`|Secret|ID of the Google Cloud Project environment as a string.|Used to configure terraform's google project variable.|
|`GCLOUD_ZONE`|Secret|Name of Google Cloud Zone.|Configures which zone to use when provisioning resources.|
|`SSH_PUBLIC_KEY`|Secret|Public SSH key for the ansible user account.|Passed to terraform job to configure provisioned nodes.|
|`SSH_PRIVATE_KEY`|Secret|Private SSH key for the ansible user account.|Added to ssh-agent during CI run.|
|`PACKAGE_URL`|Public|URL pointing at a GitLab package to install.|Allows arbitrary GitLab versions to be installed on the provisioned nodes. Defaults to current Omnibus Job.|
|`PACKAGE_REPOSITORY`|Secret|Package repository to use if not using Community or Enterprise Edition.|Allows alternative package cloud repository configurations.|
|`PACKAGE_REPOSITORY_URL`|Secret|URL to alternative packagecloud repository.|Points to the new repository defined in `PACKAGE_REPOSITORY`.|
|`GL_PRIVATE_TOKEN`|Secret|String containing a GitLab access token.|Used to download the package defined in `PACKAGE_URL`|

## Configuring Terraform

The pipeline configures the variables noted below using their definitions as
specified in the [terraform job README](../terraform#configuration-variables-for-terraform).

- `TF_VAR_google_project`
- `TF_VAR_google_zone`
- `TF_VAR_prefix`
- `TF_VAR_ssh_user`
- `TF_VAR_ssh_public_key`
- `TF_VAR_consul_count`
- `TF_VAR_database_count`
- `TF_VAR_application_count`

## Running Terraform

The [terraform job README](../terraform) describes how to stand up or
tear down an environment. It covers the necessary options for executing
[terraform][tfhome] manually or via the pipeline.

[gcred]: https://cloud.google.com/iam/docs/creating-managing-service-account-keys
[tfhome]: https://www.terraform.io
